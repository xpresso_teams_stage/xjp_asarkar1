import os
import sys
import types
import pickle
import marshal
import math
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


## $xpr_param_component_name = data_show
## $xpr_param_global_variables = ["MOUNT_PATH"]
## $xpr_param_component_type = pipeline_job


MOUNT_PATH = "/data"

x = np.arange(0, math.pi*2, 0.05)
y = np.sin(x)
plt.plot(x,y)
plt.xlabel("angle")
plt.ylabel("sine")
plt.title('sine wave')


output_folder="xjp_images/data_show"

if not os.path.exists(output_folder):
  os.makedirs(output_folder)
            
# Target method
xpresso_save_plot("image_1", output_path=MOUNT_PATH,output_folder=output_folder)

        
      


